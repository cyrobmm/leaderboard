import { shallowMount } from "@vue/test-utils";

import LeaderBoardList from "@/components/LeaderBoardList.vue";

let wrapper;

describe("LeaderBoardList.vue", () => {
  beforeEach(() => {
    wrapper = shallowMount(LeaderBoardList, {
      propsData: {
        players: []
      }
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("emits update when button decrease is clicked and player has more than 0 points", async () => {
    await wrapper.setProps({
      players: [
        {
          id: 0,
          name: "Emma",
          points: 2
        }
      ]
    });

    const btnDecrease = wrapper.get(".t-btn-decrease");
    btnDecrease.trigger("click");

    // emits
    expect(wrapper.emitted("update")).toBeTruthy();

    // assert event count
    expect(wrapper.emitted("update").length).toBe(1);
  });

  it("disables decrease button when player has 0 points and doesn't emit update when clicked", async () => {
    await wrapper.setProps({
      players: [
        {
          id: 0,
          name: "Emma",
          points: 0
        }
      ]
    });

    const btnDecrease = wrapper.get(".t-btn-decrease");
    btnDecrease.trigger("click");

    expect(btnDecrease.attributes("disabled")).toEqual("disabled");

    // emits
    expect(wrapper.emitted("update")).toBeFalsy();
  });

  it("emits update when button increase is clicked", async () => {
    await wrapper.setProps({
      players: [
        {
          id: 0,
          name: "Emma",
          points: 1
        }
      ]
    });

    const btnIncrease = wrapper.get(".t-btn-increase");
    btnIncrease.trigger("click");

    // emits
    expect(wrapper.emitted("update")).toBeTruthy();

    // assert event count
    expect(wrapper.emitted("update").length).toBe(1);
  });
});
