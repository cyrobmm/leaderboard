import { shallowMount } from "@vue/test-utils";

import LeaderBoard from "@/components/LeaderBoard.vue";
import LeaderBoardList from "@/components/LeaderBoardList.vue";

let wrapper;

describe("LeaderBoard.vue", () => {
  beforeEach(() => {
    wrapper = shallowMount(LeaderBoard, {
      data() {
        return {
          players: [
            {
              id: 0,
              name: "Emma",
              points: 0
            },
            {
              id: 1,
              name: "Noah",
              points: 10
            },
            {
              id: 2,
              name: "James",
              points: 10
            }
          ]
        };
      }
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("has LeaderBoardList component", () => {
    const leaderBoardList = wrapper.findComponent(LeaderBoardList);
    expect(leaderBoardList.exists()).toBe(true);
  });

  it("sorts array placing the player with highest point at first", () => {
    expect(wrapper.vm.players[0].points).toEqual(10);
  });

  it("sorts tied players alphabetically, ", () => {
    expect(wrapper.vm.players[0].name).toEqual("James");
  });

  it("updates player's point up by 1 on increase", () => {
    wrapper.vm.updatePlayerPoints(wrapper.vm.players[0].id, "increase");
    expect(wrapper.vm.players[0].points).toEqual(11);
  });

  it("updates player's point down by 1 on decrease", () => {
    wrapper.vm.updatePlayerPoints(wrapper.vm.players[0].id, "decrease");
    expect(wrapper.vm.players[0].points).toEqual(10);
  });
});
