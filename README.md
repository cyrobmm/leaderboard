# leaderboard

Requirements

- You have 5 users showing in a leaderboard
- All users start with 0 points
- As you click +/-, the leaderboard updates and users are re-ordered based on score
- Names sorted alphabetically if they are tied

Extra added:

- Points persists after page reload 
- Reset button to set all points back to zero

Tests:

- All tests cover mainly the two last requirements order

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```
